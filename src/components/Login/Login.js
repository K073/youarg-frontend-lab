import React, {Component} from 'react';
import 'bootstrap-social';
import 'react-bootstrap';
import {Glyphicon} from "react-bootstrap";


const wellStyles = { maxWidth: 400, margin: '0 auto 10px' };

class Login extends Component{
  render() {
    return (
      <div className="well" style={wellStyles}>
        <a href='http://lebedev.ddns.net:8000/user/vk/login' className="btn btn-block btn-social btn-vk">
          Sign in with VK
        </a>
        <a href='http://lebedev.ddns.net:8000/user/facebook/login' className="btn btn-block btn-social btn-facebook">
          Sign in with Facebook
        </a>
      </div>
    );
  }
};

export default Login;