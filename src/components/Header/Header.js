import React, {Component} from 'react'
import {FormControl, Nav, Navbar, NavItem} from "react-bootstrap";
import {Link} from "react-router-dom";
import HeaderProfile from "./HeaderProfile/HeaderProfile";

class Header extends Component{
  state = {
    searchInput: '',
    userLogin: false
  };

  searchHandler = e => {
    this.setState({searchInput: e.target.value})
  };

  render() {
    return (
      <Navbar collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}>YouArg</Link>
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <Navbar.Text>
              <Link to={'/'}>Tags</Link>
            </Navbar.Text>
          </Nav>
          <Navbar.Form pullLeft>
            <FormControl
              type='text'
              value={this.state.searchInput}
              placeholder='Enter Text'
              onChange={this.searchHandler}
            />
          </Navbar.Form>
          <Nav pullRight>
            <HeaderProfile logout={this.props.logout} userLogin={this.props.userLogin} userData={this.props.userData} />
            <Navbar.Text>
              <Link to={'/'}>Help</Link>
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default Header