import React from 'react';
import {Badge, MenuItem, Navbar, NavDropdown, NavItem} from "react-bootstrap";
import {Link} from "react-router-dom";
import Wraper from "../../../hoc/Wraper";

class HeaderProfile extends React.Component {


  render() {
    return (

    <Wraper>
      {!this.props.userLogin ? <Navbar.Text><Link to={'/login'}>Login</Link></Navbar.Text>:
        <Wraper>
          <Navbar.Text>
            <NavDropdown
              title={this.props.userData.first_name + ' ' + this.props.userData.last_name}
              id="basic-nav-dropdown">
              <MenuItem>Profile</MenuItem>
              <MenuItem divider />
              <MenuItem onClick={this.props.logout}>Logout</MenuItem>
            </NavDropdown>
          </Navbar.Text>
          <Navbar.Text>Karma <Badge>{this.props.userData.karma_cached}</Badge></Navbar.Text>
        </Wraper>
        }
    </Wraper>
    );
  }

}

export default HeaderProfile;