import React from 'react';
import axios from 'axios';
import {Button, Col, Grid, Row} from "react-bootstrap";
import PageHeader from "react-bootstrap/es/PageHeader";
import Wraper from "../../hoc/Wraper";
import Node from "./Node/Node";
import CreateNode from "./CreateNode/CreateNode";

class NodeBuilder extends React.Component {
  state = {
    nodeList: [],
    modalCreateNodeState: false
  };

  getParentList = () => {
    axios.get('http://lebedev.ddns.net:8000/node/parents/').then(res => {
      this.setState(prevState => {
        return prevState.nodeList = res.data.results
      });
      console.log(res.data);
    })
  };

  createNode = (text, argument) => {
    axios({
      method: 'post',
      url: 'http://lebedev.ddns.net:8000/node/',
      headers: {
        'Authorization': 'Token ' + localStorage.getItem('token'),
      },
      data: {
        text: text,
        argument: argument
      }
    }).then(res => {
      this.getParentList();
    })
  };

  modalNodeToggle = () => {
    this.setState({modalCreateNodeState: !this.state.modalCreateNodeState})
  };

  componentDidMount () {
    this.getParentList()
  }

  render () {
   return(
     <div className={'container'}>
       <Grid>
         <Row>
           <Col xs={12}>
             <PageHeader>
               YouArg <small> if u have args</small><Button onClick={this.modalNodeToggle} style={{float: 'right'}} bsStyle={'primary'}>Add node</Button>
             </PageHeader>
           </Col>
         </Row>
       </Grid>
       <Grid>
         <Row className="show-grid">
           <Col xs={12}>
             {this.state.nodeList.map(value => <Node key={value.pk} more={'/node-detail/' + value.pk} title={value.created}>{value.text}</Node>)}
           </Col>
         </Row>
       </Grid>
       <CreateNode create={this.createNode} close={this.modalNodeToggle} display={this.state.modalCreateNodeState}/>
     </div>
   );
  }
}

export default NodeBuilder;