import React from 'react';
import * as axios from "axios";

class NodeDetail extends React.Component {
  state = {
    data: {},
    detailList: []
  };

  getNodeDetail = () => {
    axios.get("http://lebedev.ddns.net:8000/node/tree/" + this.props.match.params.id + '/')
      .then(res => {
        this.setState(prevState => {
          return prevState.detailList = res.data.results;
        })
      })
  };

  getNode = () => {
    axios.get("http://lebedev.ddns.net:8000/node/" + this.props.match.params.id + '/')
      .then(res => {
        this.setState(prevState => {
          return prevState.data = res.data;
        })
      })
  };

  componentDidMount () {
    this.getNodeDetail();
    this.getNode();
  }
  render() {
    return(
      <div className='container'>
        <div className="panel panel-default">
          <div className="panel-heading">{this.state.data.created}</div>
          <div className="panel-body">
            {this.state.data.text}
          </div>
        </div>
        {this.state.detailList.map(value => {
          return (
            <div className="panel panel-default">
              <div className="panel-body">
                {value.text}
              </div>
            </div>
          )})}
      </div>
    )
  }
}

export default NodeDetail;