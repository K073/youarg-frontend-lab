import React from 'react'
import {Link} from "react-router-dom";

const Node = props => {
  return (
    <div className="panel panel-default">
      <div className="panel-heading">{props.title}</div>
      <div className="panel-body">
        {props.children}
      </div>
      <div className="panel-footer text-right">
        <Link to={props.more}>Read more</Link>
      </div>
    </div>
  );
};

export default Node