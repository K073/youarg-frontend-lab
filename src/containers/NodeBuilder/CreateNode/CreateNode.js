import React from 'react';
import {Button, ControlLabel, FormControl, FormGroup, HelpBlock, Modal} from "react-bootstrap";

class CreateNode extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: '',
      argument: false
    };
  }

  getValidationState() {
    const length = this.state.value.length;
    if (length > 10) return 'success';
    else if (length > 5) return 'warning';
    else if (length > 0) return 'error';
    return null;
  }

  handleChange = event => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  sendForm = event => {
    event.preventDefault();

    this.props.create(this.state.value, this.state.argument);
    this.props.close();
  };

  render() {
    return (
      <div hidden={!this.props.display} className="static-modal">
        <form onSubmit={this.sendForm}>
          <Modal.Dialog>
            <Modal.Header>
              <Modal.Title>Create node</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <FormGroup
                controlId="formBasicText"
                validationState={this.getValidationState()}
              >
                <ControlLabel>Node text</ControlLabel>
                <FormControl
                  type="text"
                  name="value"
                  value={this.state.value}
                  placeholder="Enter text"
                  onChange={this.handleChange}
                />
                <FormControl.Feedback/>

                <div className="checkbox">
                  <label>
                    <input
                      name="argument"
                      type="checkbox"
                      value={this.state.argument}
                      onChange={this.handleChange}
                    />
                    it's argument?</label>
                </div>
              </FormGroup>
            </Modal.Body>

            <Modal.Footer>
              <Button onClick={this.props.close}>Close</Button>
              <Button bsStyle="primary" type='submit'>Add</Button>
            </Modal.Footer>
          </Modal.Dialog>
        </form>
      </div>
    );
  }
}

export default CreateNode;