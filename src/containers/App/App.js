import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import './App.css';
import Header from "../../components/Header/Header";
import Login from "../../components/Login/Login";
import NodeBuilder from "../NodeBuilder/NodeBuilder";
import axios from "axios/index";
import NodeDetail from "../NodeBuilder/NodeDetail/NodeDetail";

class App extends Component {
  state = {
    userLogin: false,
    userData: {}
  };

  getUserData = () => {
    axios({
      method: 'get',
      url: 'http://lebedev.ddns.net:8000/user/self',
      headers: {
        'Authorization': 'Token ' + localStorage.getItem('token'),
      }
    }).then(res => {
      this.setState(prevState => {
        return prevState.userData = res.data;
      });
      this.setState(prevState => {
        return prevState.userLogin = true;
      })
    }).catch(err => {
      this.setState(prevState => {
        return prevState.userLogin = false;
      })
    })
  };

  logout = () => {
    localStorage.setItem('token', null);
    this.setState(prevState => {
      return prevState.userData = {};
    });
    this.setState(prevState => {
      return prevState.userLogin = false;
    })
  };

  componentDidMount () {
    const url = new URL(window.location);
    const params = new URLSearchParams(url.search);
    const token = params.get('token');
    if (localStorage.getItem('token') !== token && token !== null){
      localStorage.setItem('token', token);
    }
    this.getUserData();
  }
  render() {
    return (
      <BrowserRouter>
        <div className='App'>
          <Header logout={this.logout} userLogin={this.state.userLogin} userData={this.state.userData}/>
          <Switch>
            <Route path='/' exact component={NodeBuilder} />
            <Route path='/login' component={Login}/>
            <Route path='/node-detail/:id' component={NodeDetail} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
